@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-header">
                        <h4>Friends List</h4>
                    </div>
                    <div class="card-body alert alert-dark" style="border-radius: 0; padding: 15px; margin: 0">
                        @forelse ($friends as $friend)
                            <div>
                                <a href="{{route('chat.show', $friend->id)}}" class="text-black-50 fr-list">
                                        {{$friend->name}}
                                    <onlineuser :friend="{{ $friend }}" :onlineusers="onlineUsers"></onlineuser>
                                </a>
                            </div>
                        @empty
                            <h5 class="text-center">You don't have any friend yet.</h5>
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="alert alert-dark">
                    <h1 class="text-uppercase pub-head">Select a friend & start experiencing the new Realtime Chat!</h1>
                    <h3 class="pub-body-title"> - Easy steps to follow -</h3>
                    <div class="pub-body">
                        <ol>
                            <li>Click on your friend's name to start a private chat. (Online friends have a green circle in front!)</li>
                            <li>You will be redirected to the private Conversation page.</li>
                            <li>Start chating with your familly and friends in realtime.</li>
                            <li>Happy CHATING <i class="fas fa-smile" style="font-size: 20px"></i>.</li>
                        </ol>
                    </div>
                    <h3 class="pub-body-title"> - Features -</h3>
                    <div class="pub-body">
                            <ol>
                                <li>Privacy & Confidentiality</li>
                                <li>Real-time messages,</li>
                                <li>Real-Time notifications,</li>
                                <li>Peer-2-Peer connection between friends, so no unauthorized access to your messages,</li>
                                <li>All messages are Highly Encrypted with the latest encryption algorithms to maintain your chat secret.</li>
                            </ol>
                    </div>
                    <h3></h3>
                    <div class="pub-foot">
                            <h5>Don't forget to give us your <strong>Feedback</strong> for more improvements</h5>
                            <hr style="width: 150px; border: 1px dashed cadetblue;">
                            <span id="copyR">&copy Daouda Sanogo - 2019 All Rights Reserved.</span>
                        </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection
