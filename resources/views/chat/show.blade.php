@extends('layouts.app')

@section('content')
    <meta name="friendID" content="{{$friend->id}}">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 offset-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>{{$friend->name}} 
                            <a href="{{route('chat')}}" class="btn btn-dark fa-pull-right"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
                        </h4>
                    </div>
                    <div class="card-body alert alert-dark" style="border-radius: 0; padding: 15px; margin: 0">
                        <chat :chats="chats" :userid="{{ Auth::user()->id }}" :friendid={{$friend->id}}></chat>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
