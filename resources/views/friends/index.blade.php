@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-header">
                        <h4>Friends List</h4>
                    </div>
                    <div class="card-body alert alert-dark" style="border-radius: 0; padding: 15px; margin: 0">
                        
                        @forelse ($friends as $friend)
                            <p>{{$friend->name}}</p> 
                        @empty
                            <h5 class="text-center">You don't have any friend yet.</h5>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
