@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Welcome to Messenger</h3>
                </div>
                <div class="card-body text-center">      
                    <a href="{{route('chat')}}" class="btn btn-primary st-ch">Start Chat</a>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        @foreach ($products as $product)
        <div class="col-md-4 col-sm-4">
                <div class="card" style="width: 18rem;">
                    <span class="priceInfo">200$</span>
                    <a href="{{route('item.show', ['id' => $product->id])}}" data-lightbox="Image" data-title="My caption">
                        <img src="{{$product->image}}" class="card-img-top" alt="{{$product->image_title}}" title="{{$product->image_title}}">
                    </a>
                    
                    <div class="card-body">
                        <h5 class="card-title" style="color: lightgoldenrodyellow">{{substr($product->name, 0, 10)}}</h5>
                        <p class="card-text" style="color: lightcyan">{{substr($product->description, 0, 100)}}</p>
                        <div style="text-align: center">
                                <a href="{{route('cart.add', ['id' => $product->id, 'name' => $product->name, 'description' => $product->description, 'image' => $product->image ,'image_title' => $product->image_title, 'price' => 200, 'qty' => 1])}}" class="btn btn-primary st-ch">Add to Cart <i class="fas fa-shopping-cart    "></i></a>
                                <a href="{{route('item.show', ['id' => $product->id])}}" class="btn btn-primary st-ch">View <i class="fas fa-eye    "></i></a>
                        </div>
                        
                    </div>
            </div>
            <br>
        </div>
        @endforeach
    </div>
</div>
@endsection
