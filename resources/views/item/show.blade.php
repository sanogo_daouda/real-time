@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    
            <div class="col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h3>Welcome to Messenger</h3>
                    </div>
                    <div class="card-body text-center">      
                        <a href="{{route('chat')}}" class="btn btn-primary st-ch">Start Chat</a>
                    </div>
                </div>
            </div>
    </div>
    <br>
    @if(isset($item))
    <h2 style="text-align: center; color: lightgoldenrodyellow; border: 1px solid lightblue; border-radius: 5px; padding: 10px; background: rgba(128, 128, 128, .5);">{{$item->name}}</h2>
    <div class="row justify-content-center">
        <div class="col-md-7 col-sm-7">
            <div class="card" style="width: 35rem;">
                <span class="priceInfo">{{$item->price}}$</span>
                <a href="{{$item->attributes->image}}" target="_blank">
                    <img src="{{$item->attributes->image}}" class="card-img-top" alt="{{$item->attributes->image_title}}" title="{{$item->attributes->image_title}}">
                </a>
                
                <div class="card-body">
                    <h5 class="card-title" style="color: lightgoldenrodyellow">{{$item->name}}</h5>
                    <p class="card-text" style="color: lightcyan">{{$item->attributes->description}}</p>
                    <div style="text-align: center">
                        @if($cart->get($item->id) !== null)
                            <a href="{{route('cart.remove', ['id' => $item->id])}}" class="btn btn-danger st-ch">Remove <i class="fas fa-trash"></i></a>
                        @else
                        <a href="{{route('cart.add', ['id' => $item->id, 'name' => $item->name, 'description' => $item->attributes->description, 'image' => $item->attributes->image ,'image_title' => $item->attributes->image_title, 'price' => $item->price, 'qty' => 1])}}" class="btn btn-primary st-ch">Add to Cart <i class="fas fa-shopping-cart    "></i></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <h2 style="text-align: center; color: lightgoldenrodyellow; border: 1px solid lightblue; border-radius: 5px; padding: 10px; background: rgba(128, 128, 128, .5);">Item Not Found.</h2>
    @endif
</div>
@endsection
