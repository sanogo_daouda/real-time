@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
    
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header text-center">
                            <h3>Welcome to Messenger</h3>
                        </div>
                        <div class="card-body text-center">      
                            <a href="{{route('chat')}}" class="btn btn-primary st-ch">Start Chat</a>
                        </div>
                    </div>
                </div>
        </div>
        <br>
        <h2 style="text-align: center; color: lightgoldenrodyellow; border: 1px solid lightblue; border-radius: 5px; padding: 10px; background: rgba(128, 128, 128, .5);">Review Your Cart Before Payment</h2>
    <div class="row">
        <div class="col-sm-7 offset-2">
            @if($cart->count() > 0)
            <table class="table table-hover table-dark">
                    <thead>
                        <tr>
                        <th scope="col">#Code</th>
                        <th scope="col">Item Name</th>
                        <th scope="col">Unit Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Total</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($cart as $cartItem)
                        <tr>
                        <th>{{$cartItem->id}}</th>
                        <td>{{$cartItem->name}}</td>
                        <td>{{$cartItem->price}}$</td>
                        <td>{{$cartItem->quantity}}</td>
                        <td>{{$cartItem->price * $cartItem->quantity}}$</td>
                        <td><a href="{{route('cart.remove',['id' => $cartItem->id])}}" class="btn btn-danger st-ch"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" style="text-align: center; padding: 20px;">
                                <span class="btn btn-primary st-ch" style="border: 1px solid lightblue; padding: 15px; font-size: 20px">
                                        Total Price: 
                                    {{\Cart::getTotal()}}$
                                </span>
                            </td>
                        </tr>
                    </tfoot>
            </table>
            @else
            <h2 style="text-align: center; color: lightgoldenrodyellow; border: 1px solid lightblue; border-radius: 5px; padding: 10px; background: rgba(128, 128, 128, .5);">Your cart is EMPTY</h2>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 offset-5" style="position: relative; right: 70px;">
                @if($cart->count() > 0)
                <a href="#" class="btn btn-primary st-ch" data-toggle="modal" data-target="#paymentModal">
                    Confirm & Place Order with CC <i class="fas fa-money-bill"></i>
                </a>
                <a href="#" class="btn btn-primary st-ch" data-toggle="modal" data-target="#paymentModal2">
                    Confirm & Place Order with Paypal <i class="fas fa-money-bill"></i>
                </a>
                @else
                <a href="/" class="btn btn-primary st-ch">
                    Take me to Shopping <i class="fa fa-shopping-cart" aria-hidden="true"></i></i>
                </a>
                @endif
        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h2 class="modal-title" id="exampleModalCenterTitle" style="font-weight: bold; font-family: 'Special Elite'; font-size: 20px">Payment <i class="fas fa-money-check    "></i></h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            
        </div>
        <form action="" method="post">
            @csrf
            <div class="modal-body">
                    <p style="text-align: center;font-weight: bold; font-family: 'Special Elite'; font-size: 20px ">We accept only Credit Card & Paypal Payments.</p> 
                
                    <div class="form-group">
                        <label for="cardNumber">CC Card Number <i class="fa fa-credit-card" style="color: black"  aria-hidden="true"></i></label>
                        <input id="cardNumber" class="form-control" type="number" placeholder="123456789012345" required>
                    </div>

                    <div class="form-group">
                        <label for="expiry">Expiry Date <i class="fa fa-credit-card" style="color: black"  aria-hidden="true"></i></label>
                        <input id="expiry" class="form-control" type="number" placeholder="02/2022" required>
                    </div>

                    <div class="form-group">
                        <label for="cvv">CVV <i class="fa fa-credit-card" style="color: black" aria-hidden="true"></i></label>
                        <input id="cvv" class="form-control" type="number" placeholder="025" required>
                    </div>

                <div style="text-align: center; font-weight: bold; font-family: 'Special Elite'; font-size: 20px ">
                    Payment Amount: {{ \Cart::getTotal()}}$
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Process Payment</button>
            </div>
        </form>
        </div>
    </div>
</div>
        

  <!-- Modal -->
  <div class="modal fade" id="paymentModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h2 class="modal-title" id="exampleModalCenterTitle" style="font-weight: bold; font-family: 'Special Elite'; font-size: 20px">Payment <i class="fas fa-money-check    "></i></h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            
        </div>
        <form action="{{route('payment.paypal')}}" method="post">

            @csrf
            <div class="modal-body">
                    <p style="text-align: center;font-weight: bold; font-family: 'Special Elite'; font-size: 20px ">We accept only Credit Card & Paypal Payments .</p> 
                
                    <div class="form-group">
                        <label for="amount">Payment Amount<i class="fa fa-credit-card" style="color: black"  aria-hidden="true"></i></label>
                        <input id="amount" class="form-control" name="amount" type="text" placeholder="" value="{{ \Cart::getTotal()}}" required readonly>
                    </div>

                <div style="text-align: center; font-weight: bold; font-family: 'Special Elite'; font-size: 20px ">
                    Payment Amount: {{ \Cart::getTotal()}}$
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Process Payment</button>
            </div>
        </form>
        </div>
    </div>
</div>
        
@endsection
