@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
        
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header text-center">
                            <h3>Welcome to Messenger</h3>
                        </div>
                        <div class="card-body text-center">      
                            <a href="{{route('chat')}}" class="btn btn-primary st-ch">Start Chat</a>
                        </div>
                    </div>
                </div>
        </div>
        <br>
        <h2 style="text-align: center; color: lightgoldenrodyellow; border: 1px solid lightblue; border-radius: 5px; padding: 10px; background: rgba(128, 128, 128, .5);">Your Shopping Cart <br>
            <a href="{{route('cart.checkout')}}" class="btn btn-primary st-ch">Checkout <i class="fas fa-shopping-basket"></i></a>
        </h2>
        
        <div class="row">
            @foreach ($cart as $cartItem)
            <div class="col-md-4 col-sm-4">
                    <div class="card" style="width: 18rem;">
                        <span class="priceInfo">{{$cartItem->price}}$</span>
                        <img src="{{$cartItem->attributes->image}}" class="card-img-top" alt="{{$cartItem->attributes->image_title}}" title="{{$cartItem->attributes->image_title}}">
                        <div class="card-body">
                            <h5 class="card-title" style="color: lightgoldenrodyellow">{{substr($cartItem->name, 0, 10)}}</h5>
                            <p class="card-text" style="color: lightcyan">{{substr($cartItem->attributes->description,0, 100)}}</p>
                            <div style="text-align: center">
                                <a href="{{route('cart.remove', ['id' => $cartItem->id])}}" class="btn btn-danger st-ch">Remove <i class="fas fa-trash    "></i></a>
                            </div>
                        </div>
                </div>
                <br>
            </div>
            @endforeach
    </div>
</div>
@endsection
