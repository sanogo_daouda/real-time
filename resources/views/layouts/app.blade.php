<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="userID" content="{{Auth::check() ? Auth::user()->id : ''}}">
    <meta name="url" content="{{url('/')}}">
    <title>{{ config('app.name', 'Real time App') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Srisakdi">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Laila">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie%20Flower">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Concert%20One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Special%20Elite">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Neuton">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body{
            background-image: url("{{ asset('img/bg-social-lg.jpg')}}");
            background-size: contain;
        }

        .navbar {
            background-color: rgba(128, 128, 128, .2);
            padding: 15px 0;
            border-bottom: 1px dashed lightblue;
        }

        .navbar .navbar-brand, .nav-link  {
            color: whitesmoke !important;
        }

        .navbar .navbar-brand, .card-header {
            font-family: 'Laila';
            font-size: 35px;
        }

        .card-header{
            color: cadetblue;
            font-weight: bolder;
        }

        .navbar .navbar-brand:hover, .nav-link:hover {
            color: wheat !important;
        }

        .username {
            font-family: 'Indie Flower';
            font-size: 15px;
        }
        
        .login {
            margin-top: 75px;
        }

        .btn-primary {
            background: rgba(95, 158, 160, .7);
            color: wheat;
            border-radius: 0;
            padding: 8px 20px;
            border: 1px solid rgba(95, 158, 160, .7);
        }

        .btn-primary:hover{
            background: rgba(95, 158, 160, 1);
            border: 1px solid rgba(95, 158, 160, 1);
        }

        label, .chat-hint{
            color: burlywood !important;
            font-family: 'Neuton';
            font-size: 16px;
        }

        .st-ch{
            font-family: 'Laila';
        }

        .fr-list {
            font-family: 'Special Elite';
            font-weight: bold;
            color: burlywood !important;
        }

        .fr-list:hover {
            color: cadetblue !important;
        }

        .conversations{
            display: none;
            display: 
        }

        .pub-head {
            text-align: center;
            padding: 15px;
            font-family: 'Special Elite';
            color: cadetblue;
            font-weight: bolder;
            z-index: 5;
        }

        .pub-foot {
            text-align: center;
            font-family: 'Courgette';
            padding: 10px;
        }

        .pub-foot #copyR {
            font-weight: 500;
            background: black;
            padding: 15px;
        }
        .pub-body-title, .pub-body, .pub-foot #copyR, .pub-foot {
            color: wheat !important;
        }

        .pub-body-title {
            font-family: 'Laila';
            font-weight: bold;
        }

        .pub-body {
            font-family: 'Concert One';
            font-weight: normal;
        }

        .alert-dark, .card-body {
            background-color: rgba(128, 128, 128, .3) !important;
        }
        
        .card{
            background-color: rgba(128, 128, 128, .3) !important;
            border-color: beige;
        }

        .modal-content {
            background: crimson;
        }

        .log-btn {
            font-size: 18px;
            font-family: 'Special Elite';
        }

        .priceInfo{
                position: absolute;
                font-size: 15px;
                display: inline-block;
                background-color: crimson;
                padding: 5px;
                color: lightgoldenrodyellow;
                font-weight: bold;
                font-family: 'Special Elite';
                border-bottom-right-radius: 8px;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">

                <a class="navbar-brand" href="{{ url('/') }}">
                    {{-- {{ config('app.name', 'Laravel') }} --}}
                    SMARTY.CO <i class="fa fa-shopping-bag" style="color: crimson" aria-hidden="true"></i>
                </a>
                <small style="color: lightgoldenrodyellow; position: absolute;font-weight: bold; font-size: 13px; top: 65px; left: 170px; font-family: 'Special Elite';">Shopping is more fun with us. <i class="fa fa-smile" aria-hidden="true"></i></small>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link log-btn" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link log-btn" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle username" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas fa-shopping-cart"></i><span class="badge badge-danger badge-pill">{{$cart->count()}}</span> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if ($cart->count())
                                         @foreach ($cart as $cartItem)
                                         <div style="text-align: left">
                                                <a class="dropdown-item" href="{{route('item.show', ['id' => $cartItem->id])}}">
                                                    <span class="badge badge-dark badge-pill">{{$cartItem->quantity}}</span> - {{$cartItem->name}}
                                                </a>
                                         </div>
                                         @if($cart->count() > 1)
                                            <hr>
                                         @endif
                                         @endforeach
                                         @if($cart->count() == 1)
                                            <hr>
                                         @endif
                                         <div style="min-width: 300px; text-align: center">
                                            <a class="btn btn-primary st-ch center" style="color: brown; border-radius: 5px; font-weight: bold" href="{{route('cart.content')}}">View Cart <i class="fas fa-shopping-bag    "></i></a>
                                            <a class="btn btn-primary st-ch center" style="color: brown; border-radius: 5px; font-weight: bold" href="{{route('cart.checkout')}}">Checkout <i class="fas fa-shopping-basket    "></i></a>
                                         </div>
                                          @else
                                        <span style="padding: 10px;">No item on the Cart.</span>
                                    @endif
                                    
                                </div>
                            </li>

                            <notification :notifications="notifications" :unreads="unreads"></notification>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle username" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @if(Session::get('success')!=null)
        <div class="alert alert-success">
            <P> <h4>{{ Session::get('success') }}</h4></P>
        </div>
    @endif

    @if(Session::get('error')!=null)
        <div class="alert alert-danger">
            <P> <h4>{{ Session::get('error') }}</h4></P>
        </div>
    @endif

        <main class="py-4">
            @yield('content')
        </main>
    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
</body>
</html>
