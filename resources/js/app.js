
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.use(require('vue-chat-scroll'))

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('chat', require('./components/Chat.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);
Vue.component('onlineuser', require('./components/OnlineUser.vue').default);
Vue.component('notification', require('./components/Notification.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        chats: '',
        onlineUsers: '',
        notifications: '',
        unreads: 0
    },

    created() {
        //Get the user and his friend ID to get the chat
        const userID = $('meta[name="userID"]').attr('content');
        const friendID = $('meta[name="friendID"]').attr('content');
        var title = document.title;
        // Request the notifications
        axios.post('/getNotifications').then((response) => {
            this.notifications = response.data[0];
            this.unreads = response.data[1];

        });
        // Listen for new Notifications
        Echo.private('App.User.' + userID).notification((notification) => {
            this.notifications.unshift(notification);
            if(this.notifications.length > 5) {
                this.notifications.pop();
            }
            this.unreads += 1 ;
            document.title = "(" + this.unreads + ") " + title;
        });

        //Checkif the user is in the show.blade.php
        if(friendID != undefined) {
            
            //Getting the chat via Axios request, which is an ajax request
            axios.post('/getChat/' + friendID).then((response) => {
                this.chats = response.data;
            });


            //Now let's listen to a broadcast event
            //we put the friendID before userID cause when receiving the userID is the friendID and viceversa
            Echo.private('Chat.' + friendID + '.' + userID)
                .listen('BroadcastChat', (e) => {
                    //Update the chats object with the new chat
                    this.chats.push(e.chat);
                })
                .listenForWhisper('typing', (e) => {
                    console.log(e)
                  })
        }

        if(userID != ''){
            Echo.join('Online')
            .here((users) => {
                //When the user is logged in we update the onlineUsers
                this.onlineUsers = users;
            })
            .joining((user) => {
                //When a new user comes online, we update the onlineusers
                this.onlineUsers.push(user)
            })
            .leaving((user) => {
                //when a user logout, we remove that user to the onlineUsers
                this.onlineUsers = this.onlineUsers.filter((u) => {u != user});
            })
        }
    },
});
