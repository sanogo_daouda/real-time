<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use App\User;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function getNotifications()
    {
        $notifications = Auth::user()->notifications;
        $unreads = Auth::user()->unreadNotifications->count();

        return [$notifications, $unreads];
    }
    

    public function markAsRead(Request $request)
    {
        
        Auth::user()->notifications()->findOrFail($request->id)->markAsRead();
        
        return "Notification Read!";
    }

    public function markAllAsRead()
    {
        Auth::user()->notifications->markAsRead();
        
        return "All notifications read.";
    }
}
