<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Cart;

class PaymentController extends Controller
{
    private $apiContext;
    public function __construct() {
        $paypal_conf = config('paypal');
        $this->apiContext = new ApiContext( new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret']
        ));

        $this->apiContext->setConfig($paypal_conf['settings']);
    }


    public function payWithPaypal(Request $request)
    {

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $items = Cart::session(Auth::user()->id)->getContent();
        $itemIndex = 1;
        $itemsArray = [];
        
        foreach ($items as $item) {
            $itemIndex = new Item();

            $itemIndex->setName($item->name)
            ->setCurrency('USD')
            ->setQuantity($item->quantity)
            ->setSku($item->id) // Similar to `item_number` in Classic API
            ->setPrice($item->price);
            
            array_push($itemsArray, $itemIndex);

            $itemIndex++;
        }

        $itemList = new ItemList();
        $itemList->setItems($itemsArray);
    
        $amount = new Amount();
        $amount->setCurrency("USD")
        ->setTotal(intval(Cart::session(Auth::user()->id)->getTotal()));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Payment description")
        ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(route('payment.status'))
        ->setCancelUrl(route('payment.status'));

        $payment = new Payment();
        $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));


        try {

            $payment->create($this->apiContext);

        } catch (\Paypal\Exception\PayPalConnectionException $ex) {

            if(config('app.debug')){
                Session::put('error', 'Session Timeout');
                return redirect('/checkout');
            } else{
                Session::put('error', 'Some Error Occur, Sorry for not being able to process your payment now!');
                return redirect('/checkout');
            }
            
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }

        Session::put('error', 'Unknown error occurred');
        return redirect('/checkout');
    }

    public function paymentStatus()
    {
        $payment_id = Session::get('paypal_payment_id');

        Session::forget('paypal_payment_id');
        
        $payerID = Input::get('PayerID');
        $token = Input::get('token');

        if(empty($payerID) || empty($token)){
            Session::put('error', 'Payment Failed');

            return redirect('/checkout');
        }

        $payment = Payment::get($payment_id, $this->apiContext);
        $execution = new PaymentExecution();

        $execution->setPayerId($payerID);

        $result = $payment->execute($execution, $this->apiContext);

        if($result->getState() == "approved"){
            Session::put('success', "Payment Success");
            
            Cart::session(Auth::user()->id)->clear();
            return redirect('/checkout');
        }else{
            Session::put('error', "Payment Faiiled");
            return redirect('/checkout');
        }

    }
}
