<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $client = new Client();
        $response = $client->request('GET', 'https://hplussport.com/api/products');

        $products = json_decode($response->getBody()) ;
        
        $cart = Cart::session(Auth::user()->id)->getContent();
        return view('home', compact('cart', 'products'));
    }
}
