<?php

namespace App\Http\Controllers;

use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Notifications\newMessage;
use Cart;
class ChatController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $friends = Auth::user()->friends();
        $cart = Cart::session(Auth::user()->id)->getContent();
        return view('chat.index', compact('cart'))->withFriends($friends);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show($friend_id)
    {
        $friend = User::findOrFail($friend_id);
        $cart = Cart::session(Auth::user()->id)->getContent();
        return view('chat.show', compact('cart'))->withFriend($friend);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function getChat($friend_id)
    {
        $chats = Chat::where(function($query) use ($friend_id){
            $query->where('user_id', Auth::user()->id)->where('friend_id', $friend_id);
        })->orWhere(function($query) use ($friend_id){
            $query->where('user_id', $friend_id)->where('friend_id', Auth::user()->id);
        })->get();

        return $chats;
    }

    public function sendChat(Request $request)
    {
        $newChat = Chat::create([
            'user_id' => $request->user_id,
            'friend_id' => $request->friend_id,
            'conversation' => $request->conversation,
        ]);

        //Notify user's friend about the new message
        User::findOrFail($newChat->friend_id)->notify(new newMessage($newChat));

        return $newChat;
    }

    
}
