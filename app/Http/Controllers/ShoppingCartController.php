<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
class ShoppingCartController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function cart()
    {
        $cart = Cart::session(Auth::user()->id)->getContent();
        return view('cart.index', compact('cart'));
    }

    public function addItem(Request $request)
    {
        $userID = Auth::user()->id;

        Cart::session($userID);

        $addToCart = Cart::session($userID)->add([
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->qty,
            'attributes' => [
                'description' => $request->description,
                'image' => $request->image,
                'image_title' => $request->image_title
            ]
        ]);

        if($addToCart){
            return redirect()->back();
        }else{
            dd("Error",$request->all());
        }
        
    }
    
    public function showItem(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', 'https://hplussport.com/api/products');

        $products = json_decode($response->getBody()) ; 
        
        
        $item = Cart::session(Auth::user()->id)->get($request->id);
        $cart = Cart::session(Auth::user()->id)->getContent();
        if(!isset($item)){
            foreach ($products as $product) {
                
                if($request->id == $product->id){
                    $object = [
                        'id' => $product->id,
                        'name' => $product->name,
                        'price' => 200,
                        'quantity' => 1,
                        'attributes' => (object)[
                            'description' => $product->description, 
                            'image' => $product->image,
                            'image_title' => $product->image_title
                        ] 
                        
                    ];
                    break;
                }
            }

            $item = (object)$object;
        }
        // return $item;

      return view('item.show', compact('item', 'cart'));
    }


    public function checkout()
    {
        $cart = Cart::session(Auth::user()->id)->getContent();
        return view('cart.checkout', compact('cart'));
    }

    public function remove(Request $request)
    {
        Cart::session(Auth::user()->id)->remove($request->id);
        return redirect()->back();
    }
}
