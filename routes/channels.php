<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

// We want to make sure that the only users who can listen to this room are the user and his friend
Broadcast::channel('Chat.{user_id}.{friend_id}', function ($user, $user_id, $friend_id) {
    return $user->id == $friend_id;
    //To make sure only the user who is logged in has acces to that chat so the friend only.
});

//Creating a new Presence Channel!
Broadcast::channel('Online', function($user){
    return $user;
});