<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->middleware('auth');
Route::get('/chat', 'ChatController@index')->name('chat');
Route::get('/chat/{friend_id}', 'ChatController@show')->name('chat.show');
Route::post('/getChat/{friend_id}', 'ChatController@getChat');
Route::post('/sendChat', 'ChatController@sendChat');
Route::post('/getNotifications', 'NotificationController@getNotifications');
Route::post('/markAsRead', 'NotificationController@markAsRead');
Route::post('/markAllAsRead', 'NotificationController@markAllAsRead');
Route::get('/addItem', 'ShoppingCartController@addItem')->name('cart.add');
Route::get('/removeItem', 'ShoppingCartController@remove')->name('cart.remove');
Route::get('/cart', 'ShoppingCartController@cart')->name('cart.content');
Route::get('/checkout', 'ShoppingCartController@checkout')->name('cart.checkout');
Route::get('/item-details', 'ShoppingCartController@showItem')->name('item.show');
Route::post('/paypal', 'PaymentController@payWithPaypal')->name('payment.paypal');
Route::get('/status', 'PaymentController@paymentStatus')->name('payment.status');